import tense.*

/**
 * Represents a verb in the Finnish language.
 */
class Verb(verb: String) { // TODO implement consonant degradation

    /**
     * The string representation of the verb.
     */
    private val string = verb.trim().lowercase()

    init {
        // validate for invalid characters
        string.forEach { character ->
            if (!charset.contains(character))
                throw IllegalArgumentException("The verb contains an illegal character: $character")
        }
    }

    /**
     * The verb category that the verb fits in. There is always one and only one category for each verb. Each ver has a
     * category, if a verb doesn't fit in any category, an [IllegalArgumentException] is thrown. The category is of type
     * [VerbType].
     */
    val verbType = suffixes.firstOrNull { suffix ->
        string.endsWith(suffix.first)
    }?.second ?: throw IllegalArgumentException("The verb's ending doesn't match any of the predefined endings.")

    /**
     * Every word in Finnish has a harmony. There are two types of vowels; front vowels: ä, ö or y, and back vowels:
     * a, o, u. A word can only contain one type of vowels. The exception are compound words. That's why the algorithm
     * here is to parse the word from the back, and as soon as we encounter a vowel that helps us determine the vowel
     * harmony, we set the word's vowel harmony.
     */
    val vowelHarmony = string.reversed().run {
        forEach {
            if (it in "äöy") return@run VowelHarmony.FRONT
            else if (it in "aou") return@run VowelHarmony.BACK
        }
        // if we loop through the chars without finding a single determining vowel, we use the BACK type as default
        VowelHarmony.BACK
    }

    /**
     * A [String] representing the verb's stem. For example, the string "puhun" (to speak) has a stem of "puhu" and then
     * there are suffixes (personal endings) added to the string to put it into its correct personal form, e.g. -n as in
     * "puhun", -t as in "puhut", -u as in "puhuu" etc.
     */
    val stem = when (verbType) {
        VerbType.TYPE_1 -> string.dropLast(1)
        VerbType.TYPE_2 -> string.dropLast(2)
        VerbType.TYPE_3 -> string.dropLast(2)
        VerbType.TYPE_4 -> string.removeRange(string.length - 2, string.length - 1)
        VerbType.TYPE_5 -> string.dropLast(2)
        VerbType.TYPE_6 -> string.dropLast(2)
    }

    val conjugation = conjugate()

    /**
     * Returns the [String] representation of the verb.
     */
    override fun toString() = string

    companion object {
        /**
         * The Finnish alphabet. It is used to verify the correctness of the verb that's passed into the constructor.
         */
        private const val charset = "aäåbcdefghijklmnoöpqrsštuvwxyzž"
    }
}

private fun Verb.conjugate(): Conjugation {
    val conj = Conjugation()

    when (verbType) {
        VerbType.TYPE_1 -> {
            conj.add(Tense.ACTIVE_PRESENT, Person.FIRST_SG, stem + "n")
            conj.add(Tense.ACTIVE_PRESENT, Person.SECOND_SG, stem + "t")
            conj.add(Tense.ACTIVE_PRESENT, Person.THIRD_SG, stem + stem.last())
            conj.add(Tense.ACTIVE_PRESENT, Person.FIRST_PL, stem + "mme")
            conj.add(Tense.ACTIVE_PRESENT, Person.SECOND_PL, stem + "tte")
            conj.add(
                Tense.ACTIVE_PRESENT,
                Person.THIRD_PL,
                stem + if (vowelHarmony == VowelHarmony.BACK) "vat" else "vät"
            )
        }

        VerbType.TYPE_2 -> {
            conj.add(Tense.ACTIVE_PRESENT, Person.FIRST_SG, stem + "n")
            conj.add(Tense.ACTIVE_PRESENT, Person.SECOND_SG, stem + "t")
            conj.add(Tense.ACTIVE_PRESENT, Person.THIRD_SG, stem)
            conj.add(Tense.ACTIVE_PRESENT, Person.FIRST_PL, stem + "mme")
            conj.add(Tense.ACTIVE_PRESENT, Person.SECOND_PL, stem + "tte")
            conj.add(
                Tense.ACTIVE_PRESENT,
                Person.THIRD_PL,
                stem + if (vowelHarmony == VowelHarmony.BACK) "vat" else "vät"
            )
        }

        VerbType.TYPE_3 -> {
            conj.add(Tense.ACTIVE_PRESENT, Person.FIRST_SG, stem + "en")
            conj.add(Tense.ACTIVE_PRESENT, Person.SECOND_SG, stem + "et")
            conj.add(Tense.ACTIVE_PRESENT, Person.THIRD_SG, stem + "ee")
            conj.add(Tense.ACTIVE_PRESENT, Person.FIRST_PL, stem + "emme")
            conj.add(Tense.ACTIVE_PRESENT, Person.SECOND_PL, stem + "ette")
            conj.add(
                Tense.ACTIVE_PRESENT,
                Person.THIRD_PL,
                stem + "e" + if (vowelHarmony == VowelHarmony.BACK) "vat" else "vät")
        }

        VerbType.TYPE_4 -> {
            conj.add(Tense.ACTIVE_PRESENT, Person.FIRST_SG, stem + "n")
            conj.add(Tense.ACTIVE_PRESENT, Person.SECOND_SG, stem + "t")
            conj.add(
                Tense.ACTIVE_PRESENT,
                Person.THIRD_SG,
                stem + if (stem[stem.length - 2] in "aä") "" else if (vowelHarmony == VowelHarmony.BACK) "a" else "ä"
            )
            conj.add(Tense.ACTIVE_PRESENT, Person.FIRST_PL, stem + "mme")
            conj.add(Tense.ACTIVE_PRESENT, Person.SECOND_PL, stem + "tte")
            conj.add(
                Tense.ACTIVE_PRESENT,
                Person.THIRD_PL,
                stem + if (vowelHarmony == VowelHarmony.BACK) "vat" else "vät"
            )
        }

        VerbType.TYPE_5 -> {
            conj.add(Tense.ACTIVE_PRESENT, Person.FIRST_SG, stem + "tsen")
            conj.add(Tense.ACTIVE_PRESENT, Person.SECOND_SG, stem + "tset")
            conj.add(Tense.ACTIVE_PRESENT, Person.THIRD_SG, stem + "tsee")
            conj.add(Tense.ACTIVE_PRESENT, Person.FIRST_PL, stem + "tsemme")
            conj.add(Tense.ACTIVE_PRESENT, Person.SECOND_PL, stem + "tsette")
            conj.add(
                Tense.ACTIVE_PRESENT,
                Person.THIRD_PL,
                stem + "tse" + if (vowelHarmony == VowelHarmony.BACK) "vat" else "vät"
            )
        }
        VerbType.TYPE_6 -> {
            conj.add(Tense.ACTIVE_PRESENT, Person.FIRST_SG, stem + "nen")
            conj.add(Tense.ACTIVE_PRESENT, Person.SECOND_SG, stem + "net")
            conj.add(Tense.ACTIVE_PRESENT, Person.THIRD_SG, stem + "nee")
            conj.add(Tense.ACTIVE_PRESENT, Person.FIRST_PL, stem + "nemme")
            conj.add(Tense.ACTIVE_PRESENT, Person.SECOND_PL, stem + "nette")
            conj.add(
                Tense.ACTIVE_PRESENT,
                Person.THIRD_PL,
                stem + "ne" + if (vowelHarmony == VowelHarmony.BACK) "vat" else "vät"
            )
        }
    }

    return conj
}

/**
 * The vowel harmony of a [Verb].
 */
enum class VowelHarmony {
    FRONT, BACK
}

/**
 * Converts a string into a [Verb] instance.
 */
fun String.toVerb() = Verb(this)