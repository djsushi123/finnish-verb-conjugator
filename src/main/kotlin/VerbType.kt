/**
 * Represents a certain [Verb] category.
 */
enum class VerbType {
    TYPE_1, TYPE_2, TYPE_3, TYPE_4, TYPE_5, TYPE_6
}

/**
 * A [Map] of all the verb suffixes used in the Finnish language. They are ordered into [Pair]s of [String]s and
 * [VerbType]s.
 */
internal val suffixes = mapOf(
    "aa" to 1,
    "ea" to 1,
    "eä" to 1,
    "ia" to 1,
    "iä" to 1,
    "oa" to 1,
    "ua" to 1,
    "yä" to 1,
    "ää" to 1,
    "öä" to 1,

    "da" to 2,
    "dä" to 2,

    "lla" to 3,
    "llä" to 3,
    "nna" to 3,
    "nnä" to 3,
    "rra" to 3,
    "rrä" to 3,
    "sta" to 3,
    "stä" to 3,

    "ata" to 4,
    "ätä" to 4,
    "ota" to 4,
    "ötä" to 4,
    "uta" to 4,
    "ytä" to 4,

    "ita" to 5,
    "itä" to 5,

    "eta" to 6,
    "etä" to 6
).map { it.key to VerbType.values()[it.value - 1] }