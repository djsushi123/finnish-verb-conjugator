fun main() {
    val verbs = setOf(
        "vanheta",
        "lämmetä"
    ).map { it.toVerb() }

    verbs.forEach { verb ->
        println("=====  VERB: $verb, STEM: ${verb.stem}  =====")
        verb.conjugation.conjugates.groupBy { it.tense }.forEach { (tense, conjugates) ->
            println("TENSE: ${tense.name}")
            conjugates.forEach { println("${it.person.pronoun} ${it.string}") }
        }
        println()
    }

}