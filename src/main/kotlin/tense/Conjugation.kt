package tense

// Conjugate holder
class Conjugation {
    private val _conjugates = mutableSetOf<Conjugate>()
    val conjugates: Set<Conjugate> = _conjugates

    fun add(conjugate: Conjugate) {
        _conjugates.add(conjugate)
    }

    fun add(tense: Tense, person: Person, string: String) {
        _conjugates.add(Conjugate(tense, person, string))
    }


}