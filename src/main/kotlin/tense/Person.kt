package tense

/**
 * Represents the person of the [Conjugate]. [FIRST_SG] for example translates to "first-person singular".
 */
enum class Person(val pronoun: String) {
    FIRST_SG("minä"),
    SECOND_SG("sinä"),
    THIRD_SG("hän"),

    FIRST_PL("me"),
    SECOND_PL("te"),
    THIRD_PL("he")
}