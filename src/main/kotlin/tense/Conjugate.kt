package tense

/**
 *  A conjugate (*con-ju-get*) is a result of conjugation. That means, after we apply all the transformations to the verb
 *  in the infinitive form, it becomes a conjugate.
 */
data class Conjugate(
    val tense: Tense,
    val person: Person,
    val string: String
)